(function($) {

ChiliBook.automatic = false;
ChiliBook.lineNumbers = false;

Drupal.behaviors.chiliHighlighter = {
  attach: function(context, settings) {
    ChiliBook.recipeFolder = settings.chiliHighlighter.recipeFolder;
    $(settings.chiliHighlighter.selector, context).once('highlight', function() {
      $(this).chili();
    });
  }
};

})(jQuery);
